-- customer mktsegment -- hardcoded secondary index
-- orders orderdate -- inbuilt secondary index -- INDEX orderidx (orderdate)
-- lineitem shipdate -- inbuild secondary index -- INDEX shipidx (shipdate)

-- customer custkey -- tuples fetched on the fly
-- orders custkey -- tuples fetched on the fly
-- lineitem orderkey -- tuples fetched on the fly
-- orders orderkey -- tuples fetched on the fly

SELECT
  lineitem.orderkey,
  sum(lineitem.extendedprice*(1-lineitem.discount)) as revenue, 
  orders.orderdate,
  orders.shippriority
FROM
  customer,
  orders,
  lineitem 
WHERE
  customer.mktsegment = 'BUILDING' and customer.custkey = orders.custkey
  and lineitem.orderkey = orders.orderkey 
  and orders.orderdate < DATE('1995-03-15')
  and lineitem.shipdate > DATE('1995-03-15')
GROUP BY lineitem.orderkey, orders.orderdate, orders.shippriority 
ORDER BY revenue desc, orders.orderdate;
