-- orders orderdate -- inbuilt secondary index -- INDEX orderidx (orderdate)

-- customer custkey -- inbuilt primary key index -- PRIMARY KEY (custkey)
-- orders custkey -- tuples fetched on the fly
-- lineitem orderkey -- inbuilt secondary key index -- INDEX orderlines (orderkey)
-- orders orderkey -- tuples fetched on the fly
-- customer nationkey -- tuples fetched on the fly
-- nation nationkey -- inbuilt primary key index -- PRIMARY KEY (nationkey) 


SELECT
  nation.name,
  sum(lineitem.extendedprice * (1 - lineitem.discount)) AS revenue 
FROM
  nation, customer, orders, lineitem
WHERE
  customer.custkey = orders.custkey
  and lineitem.orderkey = orders.orderkey
  and customer.nationkey = nation.nationkey 
  and orders.orderdate >= DATE( '1994-01-01')
  and orders.orderdate < DATE ('1994-02-01')
GROUP BY nation.name
ORDER BY revenue desc;

