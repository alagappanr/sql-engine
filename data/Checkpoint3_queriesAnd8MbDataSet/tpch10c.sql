-- orders orderdate -- inbuilt secondary index -- INDEX orderidx (orderdate)
-- lineitem returnflag -- hardcoded secondary index

-- customer custkey -- tuples fetched on the fly
-- orders custkey -- tuples fetched on the fly
-- lineitem orderkey -- tuples fetched on the fly
-- orders orderkey -- tuples fetched on the fly
-- customer nationkey -- tuples fetched on the fly
-- nation nationkey -- inbuilt primary key index -- PRIMARY KEY (nationkey) 

select customer.custkey, sum(lineitem.extendedprice * (1 - lineitem.discount)) as revenue, customer.acctbal, nation.name, customer.address, customer.phone, customer.comment
from customer, orders, lineitem, nation
where customer.custkey = orders.custkey
and lineitem.orderkey = orders.orderkey
and orders.orderdate >= date('1995-06-05')
and orders.orderdate < date('1995-09-05')
and lineitem.returnflag = 'R'
and customer.nationkey = nation.nationkey
group by customer.custkey, customer.acctbal, customer.phone, nation.name, customer.address, customer.comment
order by revenue asc
limit 20;
