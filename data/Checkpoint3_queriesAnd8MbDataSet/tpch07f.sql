-- lineitem shipdate -- inbuild secondary index -- INDEX shipidx (shipdate)

-- supplier suppkey -- inbuilt primary key index -- PRIMARY KEY (suppkey)
-- lineitem suppkey -- tuples fetched on the fly
-- orders orderkey -- inbuilt primary key index -- PRIMARY KEY (orderkey)
-- lineitem orderkey -- tuples fetched on the fly
-- customer custkey -- inbuilt primary key index -- PRIMARY KEY (custkey)
-- orders custkey -- tuples fetched on the fly
-- supplier nationkey -- tuples fetched on the fly
-- nation n1 nationkey -- inbuilt primary key index -- PRIMARY KEY (nationkey) 
-- customer nationkey -- tuples fetched on the fly
-- nation n1 nationkey -- inbuilt primary key index -- PRIMARY KEY (nationkey) 

select suppnation, custnation, sum(volume) as revenue
from (
select n1.name as suppnation, n2.name as custnation, lineitem.extendedprice * (1 - lineitem.discount) as volume
from supplier, lineitem, orders, customer, nation n1, nation n2
where supplier.suppkey = lineitem.suppkey
and orders.orderkey = lineitem.orderkey
and customer.custkey = orders.custkey
and supplier.nationkey = n1.nationkey
and customer.nationkey = n2.nationkey
and lineitem.shipdate >= date('1995-03-01') 
and lineitem.shipdate <= date('1996-12-28')
) as shipping
group by
suppnation,
custnation
order by
suppnation,
custnation;
